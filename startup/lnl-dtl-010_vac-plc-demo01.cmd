# @field PLCNAME
# @type STRING
# asyn port name for the PLC

# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)


# Call the EEE module responsible for configuring IOC to PLC comms configuration
requireSnippet(s7plc-comms.cmd, "PLCNAME=$(PLCNAME), IPADDR=$(IPADDR), S7DRVPORT=2000, MODBUSDRVPORT=502, INSIZE=72, OUTSIZE=0, BIGENDIAN=1, RECVTIMEOUT=$(RECVTIMEOUT)")

dbLoadRecords("lnl-dtl-010_vac-plc-demo01.db", "PLCNAME=$(PLCNAME)")
